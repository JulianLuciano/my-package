# CHANGELOG



## v0.2.1 (2024-04-08)

### Fix

* fix(deploy): add deploy pipeline ([`311f8fd`](https://gitlab.com/JulianLuciano/my-package/-/commit/311f8fdb57127800a0f17ce3ad4ca234ffc5097f))


## v0.2.0 (2024-04-08)

### Feature

* feat(addition): add addition feature ([`c989e91`](https://gitlab.com/JulianLuciano/my-package/-/commit/c989e9144aa771453516403c91d19442748c045d))


## v0.1.0 (2024-04-08)

### Feature

* feat(sr): add semantic release cd pipeline ([`343cf5e`](https://gitlab.com/JulianLuciano/my-package/-/commit/343cf5e9e1a2b49dbd571d2042e0ce63ec412c1c))

### Unknown

* add semantic_release config ([`2d321e9`](https://gitlab.com/JulianLuciano/my-package/-/commit/2d321e9305e958d5f12fc89df99f61b3d0aacbba))

* add python-semantic-release dev dependency ([`1dbd150`](https://gitlab.com/JulianLuciano/my-package/-/commit/1dbd1501a64dd5309b780b6c12b976e60e032c68))

* Add pyproject.toml to init poetry ([`6cb11da`](https://gitlab.com/JulianLuciano/my-package/-/commit/6cb11da725976b058729b6fee07b37e7745966ad))

* Update .gitlab-ci.yml file ([`3429502`](https://gitlab.com/JulianLuciano/my-package/-/commit/34295023c0fc37187979d9cb1f95090bae4f28ad))

* Add first pipeline ([`7333465`](https://gitlab.com/JulianLuciano/my-package/-/commit/7333465a484108f47343fdb6f94f8d40088890fc))

* Initial commit ([`27afac0`](https://gitlab.com/JulianLuciano/my-package/-/commit/27afac07a62249871950dcef023fbddf390eecef))
